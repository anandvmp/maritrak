# Angular + Leaflet Map component for KSINC - Maritrak

This project contains simple `Angular 10` app with `Leaflet` map library.

## Tasks 
1. - [ ] Integrate with the directus API for location
2. - [ ] Have a controller for timeframe scrollbard for the marker (no Need to navigation lines)

## Run

Run `ng serve` for a dev server.
 
Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project.
